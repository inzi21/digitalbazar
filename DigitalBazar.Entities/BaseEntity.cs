﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBazar.Entities
{
    public class BaseEntity
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String Destription { get; set; }
    }
}
