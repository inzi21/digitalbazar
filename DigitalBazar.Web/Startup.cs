﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DigitalBazar.Web.Startup))]
namespace DigitalBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
